{
	"state": "数据可用",
	"softname": "王者荣耀",
	"Version": "4.1.0.00",
	"share_apk_lanzou": "https://appliangjian.lanzouq.com/i8F2Y06bpxyf",
	"share_apk_bilnn": "https://pan.bilnn.com/api/v3/file/sourcejump/XrGOlzFd/Ab3OSlt_htXAApUIyLMe0lAULHmKPTjFmcuyjgE8Txw*",
	"api_lanzou": "https://pan.lanzouq.com",
	"note": {
		"state": "close",
		"title": "公告标题",
		"content": "公告内容",
		"yes_link": "提示框确定打开的链接",
		"cancel_link": "提示框取消打开的链接"
	},
	"share_folderlist_lanzou": "https://pan.bilnn.com/s/Jww3HM",
	"share_folderlist_bilnn": "https://pan.bilnn.com/s/Jww3HM",
	"logs": [{
			"type": "last",
			"ver": "4.1.0.00",
			"title": "更新日志",
			"date": "2022.06.12",
			"text": [
				"1.更新 全新架构；",
				"2.增加 小姐姐短视频；",
				"3.增加 更多福利资源站点;"
				
			]
		},
		{
			"type": "history",
			"ver": "3.4.2.00",
			"title": "更新日志",
			"date": "2022.04.27",
			"text": [
				"1.更新 更新插件的部分线路；",
				"2.增加 播放界面 增加重新解析视频功能；",
				"3.优化 优化视频播放错误后自动切换内核的逻辑错误;",
				"4.其它 播放窗口代码优化;"
			]
		},
		{
			"type": "history",
			"ver": "3.4.1.00",
			"title": "更新日志",
			"date": "2022.01.11",
			"text": [
				"1.更新 更新插件的部分线路；",
				"2.增加 播放界面增加重新解析视频功能；",
				"3.优化 优化视频播放错误后自动切换内核的逻辑错误;",
				"4.其它 播放窗口代码优化;"
			]
		},
		{
			"ver": "3.4.0.10",
			"title": "更新日志",
			"date": "2021-12-11",
			"text": [
				"1.修复 失去焦点后仍会自动播放下一集的BUG；",
				"2.修复 自动播放下一集返回详情页选集错误的BUG；",
				"3.优化 一些细节逻辑,提高运行效率；"
			]
		},
		{
			"ver": "3.3.0.10",
			"title": "更新日志",
			"date": "2021-11-29",
			"text": [
				"1.修复 播放器界面选集错误的BUG；",
				"2.增加 自动播放下一集功能；",
				"3.优化 部分界面UI显示不完整；",
				"4.增加 自动进入全屏功能"
			]
		}
	],

	"Copyright": "本程序可自由复制、传播，但必须保持程序的格式完整性，如需对程序进行修改，请保留软件原作者标识，不得用于商业目的或非法目的。"
}
